This document explains how to install and use DNA-ML package.
### Setup ###

In order to install DNA-ML package, you should already have a Python 2.7 installation. If you do not have Python installed, we recommend [Anaconda](https://www.continuum.io/why-anaconda). After downloading DNAML package, open a shell inside the directory and run
```bash
python setup.py install
```
The setup script will download required packages and install them as well.
### Usage ###
After installation, to use the package with the supplied training data, run
```bash
dnaml-run [prediction_path]
```

If you want to supply your own training data, use
```bash
dnaml-run [prediction_path] [training_path]
```

For multiclass prediction, add -m to the command, for example
```bash
dnaml-run -m [prediction_path] [training_path]
```