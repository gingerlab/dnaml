"""unsupervised.py: Unsupervised learning for DNA pull curves."""

__author__ = "Durmus U. Karatay"
__copyright__ = "Copyright 2015, Ginger Lab"
__maintainer__ = "Durmus U. Karatay"
__email__ = "ukaratay@uw.edu"
__status__ = "Development"

import numpy as np
from dnaml.utils import io, dna, preprocessing
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
from sklearn.mixture import GMM
from sklearn.metrics import accuracy_score, f1_score

# Parameters for what we are analyzing.
conditions = ['initial', 'uv']
forces = [20, 50, 200, 500, 1000, 2000]  # nm/s

# Number of tests to do.
folds = 50

# Keep scores.
accuracy = np.zeros((len(conditions), len(forces), folds))
f1 = np.zeros((len(conditions), len(forces), folds))

for i, condition in enumerate(conditions):

    for j, force in enumerate(forces):

        print '{0:s}, {1:d}'.format(condition, force)

        # Get the path.
        dir_path = '../data/dk/{0:s}/{1:d}'.format(condition, force)

        # Load and binarize.
        X, y = io.load_library(dir_path)
        y = preprocessing.binarize(y)

        # Find flat ones and remove from the set.
        fp = dna.ForcePeaks()
        X_fp = fp.fit_transform(X)

        idx = np.where(X_fp[:, 1] == 0)[0]
        idx_keep = np.where(X_fp[:, 1] != 0)[0]
        X = np.delete(X, idx, axis=0)
        y_del = np.delete(y, idx, axis=0)

        # Initialize PCA.
        pca = PCA(n_components=0.90)
        X_pca = pca.fit_transform(X)
        X_pca_scaled = scale(X_pca)

        # Create the new prediction.
        y_pred_new = np.zeros(200)

        for k in range(folds):

            # Initialize GMM with 2 components and fit with scaled components.
            clf = GMM(n_components=2, covariance_type='full')
            clf.fit(X_pca_scaled)
            y_pred = clf.predict(X_pca_scaled)

            if accuracy_score(y_del, y_pred) < 0.5:

                y_pred = np.logical_not(y_pred) * 1

            y_pred_new[idx_keep] = y_pred

            accuracy[i, j, k] = accuracy_score(y, y_pred_new)
            f1[i, j, k] = f1_score(y, y_pred_new)

            print 'Fold {0:d}, {1:.2f}'.format(k, accuracy[i, j, k])

# Calculate metrics.
accuracy_m = accuracy.mean()
accuracy_s = accuracy.std()

f1_m = f1.mean()
f1_s = f1.std()

print
print 'Accuracy {0:.3f} +/- {1:.3f}'.format(accuracy_m, accuracy_s)
print 'F1       {0:.3f} +/- {1:.3f}'.format(f1_m, f1_s)
print
