#pragma rtGlobals=3		// Use modern global access method and strict wave access.

function classify(folderName)

	// Create the data path from folder name.
	string folderName
       setDataFolder root:
	string dataFolder  = "root:ForceCurves:Display:SubFolders:" + folderName + ":"

	// Get and set some useful variables.
	variable i
	variable class = 1
	string currentName, forceName, zsnsrName
	newpath savepath

	// This is the list that the analysis software keeps.
	wave/t list = root:ForceCurves:Parameters:SlaveFPList
	variable numCurves = Dimsize(list, 0) - 1	
	
	make/o/n=(numCurves) classes  // Initialize the label array to the number of curves.
	
	// Go into the data folder where the force curves are.
	SetDataFolder dataFolder
	
	// Loop over all the curves and get them classified.
	for(i = 0; i < numCurves; i += 1)
	
		SplitString/E="^\\s*(.*?)\\s*$" list[i + 1], currentName // Strip all whitespace from the curve names before splicing.
		forceName = currentName + "Force_Ret"
		zsnsrName = currentName + "Zsnsr_Ret"
		
		// Get the class for current force curve from the user.
		prompt class, "Class:"
		DoPrompt "Enter Class:", class
		classes[i] = class
		
		// Save force data.
		save/j/o/p=savepath $zsnsrName, $forceName as currentName + ".txt"
		
		// If this was the last curve, stop. Otherwise move to the next curve.
		if(i < numCurves - 1)
			
			ForceDisplayButtonProc("Next1ForceButton_0")
		
		endif
	
	endfor
	
	// Go back to where we started.
	SetDataFolder root:
	
	// Save the labels as well.
	save/j/o/p=savepath classes as "Classes.txt"

end

function save_forces(folderName)

	// Create the data path from folder name.
	string folderName
	string dataFolder  = "root:ForceCurves:Display:SubFolders:" + folderName + ":"

	// Get and set some useful variables.
	string currentFolder = GetDataFolder(1)

	variable i
	string currentName, forceName, zsnsrName
	newpath savepath

	// This is the list that the analysis software keeps.
	wave/t list = root:ForceCurves:Parameters:SlaveFPList
	variable numCurves = Dimsize(list, 0) - 1	
	
	// Go into the data folder where the force curves are.
	SetDataFolder dataFolder
	
	// Loop over all the curves and get them classified.
	for(i = 0; i < numCurves; i += 1)
	
		SplitString/E="^\\s*(.*?)\\s*$" list[i + 1], currentName // Strip all whitespace from the curve names before splicing.
		forceName = currentName + "Force_Ret"
		zsnsrName = currentName + "Zsnsr_Ret"
		
		// Save force data.
		save/j/o/p=savepath $zsnsrName, $forceName as currentName + ".txt"
		
		// If this was the last curve, stop. Otherwise move to the next curve.
		if(i < numCurves - 1)
			
			ForceDisplayButtonProc("Next1ForceButton_0")
		
		endif
	
	endfor
	
	// Go back to where we started.
	SetDataFolder currentFolder

end