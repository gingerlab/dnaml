"""comparator_test.py: Compare users' labels and report."""

__author__ = "Durmus U. Karatay"
__copyright__ = "Copyright 2015, Ginger Lab"
__email__ = "ukaratay@uw.edu"

import numpy as np
from dnaml.utils import preprocessing
from sklearn.cross_validation import StratifiedShuffleSplit
from sklearn.metrics import accuracy_score, f1_score

# Lists for what we are analyzing.
ground_truth = 'dk'
users = ['jz', 'jgl', 'lf']
conditions = ['initial', 'uv']
forces = [20, 50, 200, 500, 1000, 2000]

# Initialize arrays
accuracy_b = np.zeros((len(users), len(conditions), len(forces)))
f1_b = np.zeros((len(users), len(conditions), len(forces)))

accuracy_m = np.zeros((len(users), len(conditions), len(forces)))
f1_m = np.zeros((len(users), len(conditions), len(forces)))

# Split every class in itself.
for i, condition in enumerate(conditions):

    for j, force in enumerate(forces):

        # Get the path for ground truth.
        dir_path = '../data/{0:s}/{1:s}/{2:d}'.format(ground_truth, condition, force)
        fname = dir_path + '/Classes.txt'

        # Load ground truth.
        y = np.loadtxt(fname, dtype=int)

        # Map classes to binary.
        y_b = preprocessing.binarize(y)
        y_m = preprocessing.map_labels(y)

        for k, user in enumerate(users):

            user_path = '../data/{0:s}/{1:s}/{2:d}'.format(user, condition, force)
            user_fname = user_path + '/Classes.txt'

            y_user = np.loadtxt(user_fname, dtype=int)
            y_user_b = preprocessing.binarize(y_user)
            y_user_m = preprocessing.map_labels(y_user)

            accuracy_b[k, i, j] = accuracy_score(y_b, y_user_b)
            f1_b[k, i, j] = f1_score(y_b, y_user_b)

            accuracy_m[k, i, j] = accuracy_score(y_m, y_user_m)
            f1_m[k, i, j] = f1_score(y_m, y_user_m, average='macro', pos_label=None)

