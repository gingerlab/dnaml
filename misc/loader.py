"""loader.py: Load libraries and save them as a binary file."""

__author__ = "Durmus U. Karatay"
__copyright__ = "Copyright 2015, Ginger Lab"
__email__ = "ukaratay@uw.edu"

import numpy as np
from dnaml.utils import io, preprocessing
from sklearn.cross_validation import StratifiedShuffleSplit

# Lists for what we are analyzing.
conditions = ['initial', 'uv']
forces = [20, 50, 200, 500, 1000, 2000]

# Initialize lists.
X_train, y_train = [], []
X_test, y_test = [], []

# Split every class in itself.
for i, condition in enumerate(conditions):

    for j, force in enumerate(forces):

        # Get the path.
        dir_path = '../data/dk/{0:s}/{1:d}'.format(condition, force)

        # Load library
        X, y = io.load_library(dir_path)
        print 'Loading the library at {0:s}'.format(dir_path)

        # Map 7 classes to binary.
        y_b = preprocessing.binarize(y)

        # Get the stratified split set up on binary classes.
        sss = StratifiedShuffleSplit(y_b, n_iter=1,
                                     test_size=0.5, random_state=1989)

        for train_idx, test_idx in sss:

            X_train.append(X[train_idx])
            y_train.append(y[train_idx])

            X_test.append(X[test_idx])
            y_test.append(y[test_idx])

# Concatenate the training set and save.
X_train = np.concatenate(X_train, axis=0)
y_train = np.concatenate(y_train)

np.savez('../data/train.npz', X=X_train, y=y_train)

# Concatenate the test set and save.
X_test = np.concatenate(X_test, axis=0)
y_test = np.concatenate(y_test)

np.savez('../data/test.npz', X=X_test, y=y_test)
