"""classifier_validation.py: Cross-validation for different classifiers."""

__author__ = "Durmus U. Karatay"
__copyright__ = "Copyright 2015, Ginger Lab"
__email__ = "ukaratay@uw.edu"

import numpy as np
from dnaml.utils import preprocessing, dna
from sklearn.pipeline import FeatureUnion
from sklearn.cross_validation import StratifiedKFold
from sklearn.metrics import accuracy_score, f1_score, log_loss

# Import all test classifiers
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB

# Load data.
with np.load('../data/train.npz') as data:

    X = data['X']
    y = data['y']

y = preprocessing.map_labels(y)

# Initialize our transformers.
transformers = FeatureUnion([('ForcePeaks', dna.ForcePeaks()),
                             ('Slope', dna.Slope(best_line=False)),
                             ('STD', dna.STD()),
                             ('Area', dna.Area()),
                             ('Moment', dna.Moments(moments=(2,), scaled=False))])

# Use all the transformers to transform raw signals.
X_new = transformers.fit_transform(X, y)

# Initialize our classifiers.
names = ['Nearest Neighbors', 'RBF SVM', 'Decision Tree',
         'Random Forest', 'AdaBoost', 'Naive Bayes']

clfs = [KNeighborsClassifier(),
        SVC(kernel='rbf', gamma=2, C=1, probability=True),
        DecisionTreeClassifier(),
        RandomForestClassifier(),
        AdaBoostClassifier(),
        GaussianNB()]

# Get the cross-validation ready.
skf = StratifiedKFold(y, n_folds=10, shuffle=True, random_state=0427)

# Keep scores.
accuracy = np.zeros((skf.n_folds, len(clfs)))
f1 = np.zeros((skf.n_folds, len(clfs)))
log_lost = np.zeros((skf.n_folds, len(clfs)))

# Reset the counter.
i = 0

print 'Starting CV!'

for train_idx, val_idx, in skf:

    # Separate the dataset.
    X_train, y_train = X_new[train_idx], y[train_idx]
    X_val, y_val = X_new[val_idx], y[val_idx]

    # Reset counter.
    j = 0

    for name, clf in zip(names, clfs):

        print name, ', Run ', i

        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_val)
        y_prob = clf.predict_proba(X_val)

        accuracy[i, j] = accuracy_score(y_val, y_pred)
        f1[i, j] = f1_score(y_val, y_pred, average='macro')
        log_lost[i, j] = log_loss(y_val, y_prob)

        j += 1

    i += 1

results = np.savez('../results/results_classifier_m.npz',
                  accuracy=accuracy,
                  f1=f1,
                  log_lost=log_lost)

# Calculate metrics.
accuracy_m = accuracy.mean(axis=0)
accuracy_s = accuracy.std(axis=0)

f1_m = f1.mean(axis=0)
f1_s = f1.std(axis=0)

log_lost_m = log_lost.mean(axis=0)
log_lost_s = log_lost.std(axis=0)

# Print the results onto screen.
for i, name in enumerate(names):

    print 'Classifier: {0:s}'.format(name)
    print 'Accuracy {0:.3f} +/- {1:.3f}'.format(accuracy_m[i], accuracy_s[i])
    print 'F1       {0:.3f} +/- {1:.3f}'.format(f1_m[i], f1_s[i])
    print 'Log loss {0:.3f} +/- {1:.3f}'.format(log_lost_m[i], log_lost_s[i])
    print
