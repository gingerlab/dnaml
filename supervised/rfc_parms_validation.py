"""rfc_parms_validation.py: Cross-validation of RFC parameters."""

__author__ = "Durmus U. Karatay"
__copyright__ = "Copyright 2015, Ginger Lab"
__email__ = "ukaratay@uw.edu"

import numpy as np
from dnaml.utils import preprocessing, dna
from sklearn.pipeline import FeatureUnion
from sklearn.grid_search import ParameterGrid
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import StratifiedKFold
from sklearn.metrics import accuracy_score, f1_score, log_loss

# Load data.
with np.load('../data/train.npz') as data:

    X = data['X']
    y = data['y']

y = preprocessing.binarize(y)

# Initialize our transformers.
transformers = FeatureUnion([('ForcePeaks', dna.ForcePeaks()),
                             ('Slope', dna.Slope(best_line=False)),
                             ('STD', dna.STD()),
                             ('Area', dna.Area()),
                             ('Moment', dna.Moments(moments=(2,), scaled=False))])

# Use all the transformers to transform raw signals.
X_new = transformers.fit_transform(X, y)

# Initialize the parameter grid for classifier.
parms = [{'n_estimators': [10, 100, 1000],
          'criterion': ['gini', 'entropy'],
          'max_features': range(2, 6),
          'min_samples_split': [1, 2],
          'random_state': [2015]}]

grid = ParameterGrid(parms)
grid_list = list(ParameterGrid(parms))

# Get the cross-validation ready.
skf = StratifiedKFold(y, n_folds=10, shuffle=True, random_state=0427)

# Keep scores.
accuracy = np.zeros((skf.n_folds, len(grid_list)))
f1 = np.zeros((skf.n_folds, len(grid_list)))
log_lost = np.zeros((skf.n_folds, len(grid_list)))

# Reset counter.
i = 0

print 'Starting CV!'

for train_idx, val_idx, in skf:

    # Separate the dataset.
    X_train, y_train = X_new[train_idx], y[train_idx]
    X_val, y_val = X_new[val_idx], y[val_idx]

    # Reset counter.
    j = 0

    for parm in grid:

        print 'Parameter ', j, ', Run ', i

        clf = RandomForestClassifier(**parm)

        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_val)
        y_prob = clf.predict_proba(X_val)

        accuracy[i, j] = accuracy_score(y_val, y_pred)
        f1[i, j] = f1_score(y_val, y_pred, average='binary')
        log_lost[i, j] = log_loss(y_val, y_prob)

        j += 1

    i += 1

results = np.savez('../results/rfc_parms_b.npz',
                    accuracy=accuracy,
                    f1=f1,
                    log_lost=log_lost)

# Calculate metrics.
accuracy_m = accuracy.mean(axis=0)
accuracy_s = accuracy.std(axis=0)

f1_m = f1.mean(axis=0)
f1_s = f1.std(axis=0)

log_lost_m = log_lost.mean(axis=0)
log_lost_s = log_lost.std(axis=0)

i = log_lost_m.argmin()

print grid_list[i]
print 'Accuracy {0:.3f} +/- {1:.3f}'.format(accuracy_m[i], accuracy_s[i])
print 'F1       {0:.3f} +/- {1:.3f}'.format(f1_m[i], f1_s[i])
print 'Log loss {0:.3f} +/- {1:.3f}'.format(log_lost_m[i], log_lost_s[i])
print
