"""rfc_test.py: Test for selected Random Forest parameters."""

__author__ = "Durmus U. Karatay"
__copyright__ = "Copyright 2015, Ginger Lab"
__email__ = "ukaratay@uw.edu"

import numpy as np
from dnaml.utils import preprocessing, dna
from sklearn.pipeline import FeatureUnion
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, f1_score, log_loss

# Load training data.
with np.load('../data/train.npz') as data:

    X_train = data['X']
    y_train = data['y']

# Load data.
with np.load('../data/test.npz') as data:

    X_test = data['X']
    y_test = data['y']

y_train = preprocessing.map_labels(y_train)
y_test = preprocessing.map_labels(y_test)

# Initialize our transformers.
transformers = FeatureUnion([('ForcePeaks', dna.ForcePeaks()),
                             ('Slope', dna.Slope(best_line=False)),
                             ('STD', dna.STD()),
                             ('Area', dna.Area()),
                             ('Moment', dna.Moments(moments=(2,), scaled=False))])

# Use all the transformers to transform raw signals.
X_train_new = transformers.fit_transform(X_train, y_train)
X_test_new = transformers.transform(X_test)

# Replace features.
X_train_new[:, 4] = X_train_new[:, 3]
X_test_new[:, 4] = X_test_new[:, 3]

# Number of tests to do.
folds = 50

# Keep scores.
accuracy = np.zeros(folds)
f1 = np.zeros(folds)
log_lost = np.zeros(folds)


for i in range(folds):

    print 'Run ', i

    clf = RandomForestClassifier(criterion='entropy',
                                 n_estimators=1000,
                                 max_features=2,
                                 min_samples_split=1)

    clf.fit(X_train_new, y_train)
    y_pred = clf.predict(X_test_new)
    y_prob = clf.predict_proba(X_test_new)

    accuracy[i] = accuracy_score(y_test, y_pred)
    f1[i] = f1_score(y_test, y_pred, average='macro')
    log_lost[i] = log_loss(y_test, y_prob)


# Calculate metrics.
accuracy_m = accuracy.mean()
accuracy_s = accuracy.std()

f1_m = f1.mean()
f1_s = f1.std()

log_lost_m = log_lost.mean()
log_lost_s = log_lost.std()

print 'Accuracy {0:.3f} +/- {1:.3f}'.format(accuracy_m, accuracy_s)
print 'F1       {0:.3f} +/- {1:.3f}'.format(f1_m, f1_s)
print 'Log loss {0:.3f} +/- {1:.3f}'.format(log_lost_m, log_lost_s)
print
