"""rfc_class_test.py: Test RF classifier for classes."""

__author__ = "Durmus U. Karatay"
__copyright__ = "Copyright 2015, Ginger Lab"
__email__ = "ukaratay@uw.edu"

import numpy as np
from dnaml.utils import preprocessing, dna
from sklearn.pipeline import FeatureUnion
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

# Load training data.
with np.load('../data/train.npz') as data:

    X_train = data['X']
    y_train = data['y']

# Initialize our transformers.
transformers = FeatureUnion([('ForcePeaks', dna.ForcePeaks()),
                             ('Slope', dna.Slope(best_line=False)),
                             ('STD', dna.STD()),
                             ('Area', dna.Area()),
                             ('Moment', dna.Moments(moments=(2,), scaled=False))])

# Use all the transformers to transform raw signals.
X_train_new = transformers.fit_transform(X_train, y_train)
y_train = preprocessing.binarize(y_train)

# Parameters for what we are analyzing.
conditions = ['initial', 'uv']
forces = [20, 50, 200, 500, 1000, 2000]  # nm/s

# Load test data.
with np.load('../data/test.npz') as data:

    X = data['X']
    y = data['y']

# Number of tests to do.
folds = 50

# Keep scores.
accuracy = np.zeros((len(conditions), len(forces), folds))
f1 = np.zeros((len(conditions), len(forces), folds))

X_conditions = np.split(X, len(conditions), axis=0)
y_conditions = np.split(y, len(conditions), axis=0)

for k in range(folds):

    print 'Run ', k

    clf = RandomForestClassifier(criterion='entropy',
                                 n_estimators=1000,
                                 max_features=2,
                                 min_samples_split=1)
    clf.fit(X_train_new, y_train)

    for i, condition in enumerate(conditions):

        X_condition_forces = np.split(X_conditions[i], len(forces), axis=0)
        y_condition_forces = np.split(y_conditions[i], len(forces), axis=0)

        for j, force in enumerate(forces):

            print '{0:s}, {1:d}'.format(condition, force)

            X_condition_force = X_condition_forces[j]
            X_condition_force_new = transformers.transform(X_condition_force)

            y_condition_force = preprocessing.binarize(y_condition_forces[j])
            y_condition_force_pred = clf.predict(X_condition_force_new)

            accuracy[i, j, k] = accuracy_score(y_condition_force, y_condition_force_pred)