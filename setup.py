from setuptools import setup, find_packages

setup(
    name='DNAML',
    version='1.0',
    description='Automatic analyzer for DNA Pulling Experiments',

    author='Durmus U. Karatay',
    author_email='ukaratay@uw.edu',
    license='MIT',

    packages=find_packages(exclude=['results', 'plotting',
                                    'supervised', 'unsupervised']),

    install_requires=['numpy>=1.9.3',
                      'scipy>=0.16.0',
                      'scikit-learn>=0.16.1'],
   package_data={
        'dnaml': ['training/*.txt']},

   entry_points={
        'console_scripts': [
            'dnaml-run = dnaml.run:main',
        ],
    },
)
