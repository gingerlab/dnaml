"""rfc.py: Contains a wrapper class for Random Forest classifier."""
# pylint: disable=E1101,R0902,C0103
__author__ = "Durmus U. Karatay"
__copyright__ = "Copyright 2015, Ginger Lab"
__email__ = "ukaratay@uw.edu"
__status__ = "Production"

import itertools
from dnaml.utils import preprocessing, dna
from sklearn.pipeline import FeatureUnion
from sklearn.ensemble import RandomForestClassifier


class RFC(object):
    """
    Random Forest Classifier for Dynamic Force Spectroscopy of DNA.

    Classifies force-distance curves acquired by an AFM with a given training
    dataset. The method is described in detail in the article by Karatay et
    al. [1]_.

    Parameters
    ----------
    feature_set : list
       Turning features on and off, by deafult all features are on. The list
       of current features is ForcePeaks, Slope, STD, Area, and Moment.

    multiclass : bool
        0 denotes binary classification, 1 denotes multiclass classification.
        Default behavior is to classify binarily.

    Attributes
    ----------
    clf : RandomForestClassifier object
        Classifier.
    features : list
        The list of features used in classification.

    Methods
    -------
    train(X, y)
        Trains the classifier from X and y.
    predict(X)
        Predicts the labels for given X.

    References
    ----------
    .. [1] Karatay D., Zhang J., et al. Classifying Force Spectroscopy of DNA
    Pulling Measurements Using Supervised and Unsupervised Machine Learning
    Methods

    Examples
    --------
    >>> from dnaml import rfc, utils
    >>>
    >>> train_path = '../data/train/'
    >>> test_path = '../data/test/'
    >>>
    >>> x_train, y_train = util.io.load_library(train_path)
    >>> x_test, y_test = util.io.load_library(test_path)
    >>>
    >>> r = rfc.RFC(multiclass=0)
    >>> r.train(x_train, y_train)
    >>> predictions = clf.predict(x_test)

    """

    def __init__(self, feature_set=[1, 1, 1, 1, 1, 1], multiclass=0):

        # Initialize our transformers.
        features = [('ForcePeaks', dna.ForcePeaks()),
                    ('Slope', dna.Slope(best_line=False)),
                    ('STD', dna.STD()),
                    ('Area', dna.Area()),
                    ('Moment', dna.Moments(moments=(2,), scaled=False))]

        # Select the ones requested by user.
        self.features = list(itertools.compress(features, feature_set))
        self.transformers = FeatureUnion(self.features)

        # Define if it is gonna be a binary or multiclass classification.
        self.multiclass = multiclass

        if self.multiclass:

            self.clf = RandomForestClassifier(criterion='entropy',
                                              n_estimators=1000,
                                              max_features=2,
                                              min_samples_split=1)
        else:

            self.clf = RandomForestClassifier(criterion='entropy',
                                              n_estimators=1000,
                                              max_features=4,
                                              min_samples_split=1)

        # Initialize the attributes.
        self.y_pred = None
        self.y_prob = None

        return

    def train(self, X, y):
        """Build a forest of trees from the training set (X, y).

        Parameters
        ----------
        X : array-like or sparse matrix of shape = [n_samples, n_features]
            The training input samples. Internally, it will be converted to
            ``dtype=np.float32`` and if a sparse matrix is provided
            to a sparse ``csc_matrix``.
        y : array-like, shape = [n_samples] or [n_samples, n_outputs]
            The target values

        Returns
        -------
        self : object
            Returns self.

        """

        if self.multiclass:

            y_new = preprocessing.map_labels(y)

        else:

            y_new = preprocessing.binarize(y)

        X_new = self.transformers.fit_transform(X, y_new)

        self.clf.fit(X_new, y_new)

        return self

    def predict(self, X):
        """"Predict class for X.

        The predicted class of an input sample is a vote by the trees in
        the forest, weighted by their probability estimates. That is,
        the predicted class is the one with highest mean probability
        estimate across the trees.

        Parameters
        ----------
        X : array-like or sparse matrix of shape = [n_samples, n_features]
            The input samples. Internally, it will be converted to
            ``dtype=np.float32`` and if a sparse matrix is provided
            to a sparse ``csr_matrix``.

        Returns
        -------
        y : array of shape = [n_samples] or [n_samples, n_outputs]
            The predicted classes.
        """

        X_new = self.transformers.transform(X)

        self.y_pred = self.clf.predict(X_new)
        self.y_prob = self.clf.predict_proba(X_new)

        return self.y_pred
