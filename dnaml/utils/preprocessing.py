"""preprocessing.py: Includes routines for preprocessing datasets."""

__author__ = "Durmus U. Karatay"
__copyright__ = "Copyright 2015, Ginger Lab"
__maintainer__ = "Durmus U. Karatay"
__email__ = "ukaratay@uw.edu"
__status__ = "Development"

import numpy as np


def binarize(y, cls0=[0, 1, 2]):
    """
    Binarizes multi-class labels.

    Parameters
    ----------
    y: (n_samples, ) array_like
        Labels to be binarized.
    cls0: array_like, optional
        Classes that are to be mapped to 0. The other labels get mapped to 1.

    Returns
    -------
    y_new: (n_samples, ) array_like
        Binarized labels.

    """

    y_new = np.copy(y)
    unique_labels = np.unique(y_new)

    for label in unique_labels:

        idx = (y_new == label)

        if label in cls0:

            y_new[idx] = 0

        else:

            y_new[idx] = 1

    return y_new


def map_labels(y, src=range(7), dst=(0, 1, 2, 3, 4, 4, 4)):
    """
    Binarizes multi-class labels.

    Parameters
    ----------
    y: (n_samples, ) array_like
        Labels to be binarized.
    src: array_like, optional
        Classes that are to be mapped from.
    dst: array_like, optional
        Classes that are to be mapped to.

    Returns
    -------
    y_new: (n_samples, ) array_like
        Mapped labels.

    """

    y_new = np.copy(y)

    if len(src) == len(dst):

        for src_label, dst_label in zip(src, dst):

            y_new[y == src_label] = dst_label

        return y_new

    else:

        print 'Destination and source are not same length.'

        return


def rearrange_labels(y):
    """
    Rearranges labels to make sure that the most counted labels are 1.
    Needed by unsupervised learning.

    Parameters
    ----------
    y: (n_samples, ) array_like
        Labels to be rearranged. It needs to be binary.

    Returns
    -------
    y_new: (n_samples, ) array_like
        Rearranged labels.

    """

    count = np.count_nonzero(y)

    if (2 * count) < len(y):

        # Invert the array, and multiply by one to convert type.
        y_new = np.logical_not(y) * 1

    else:

        y_new = y

    return y_new
