"""io.py: Includes routines for interfacing with datasets."""

__author__ = "Durmus U. Karatay"
__copyright__ = "Copyright 2015, Ginger Lab"
__maintainer__ = "Durmus U. Karatay"
__email__ = "ukaratay@uw.edu"
__status__ = "Production"

import re
import os
import numpy as np
from scipy.interpolate import interp1d


def __tryint__(string):
    """Tries if a string is integer or not."""

    try:

        return int(string)

    except:

        return string


def __alphanum_key__(string):
    """Turns a string into a list of string and number chunks."""

    return [__tryint__(char) for char in re.split('([0-9]+)', string)]


def load_library_labels(dir_list):
    """
    Loads only labels for different libraries and concatenates them.

    Parameters
    ----------
    dir_list : array
        List of directiories to load labels from.

    Returns
    -------
    y : (n_samples_total, ) array_like
        Classes

    See also
    --------
    io.load_libraries : Loads a single library.

    """

    # Start going through the list.
    for i, dir_path in enumerate(dir_list):

        fname = dir_path + '/Classes.txt'
        y = np.loadtxt(fname, dtype=int)

        if i == 0:

            y0 = np.copy(y)

        else:

            y0 = np.hstack((y0, y))

    return y0


def load_signal(fname, bounds=(-25, 100), n_points=500):
    """
    Loads a tab-delimited text file that has z-sensor (in meters) and force (in
    Newtons) data and returns force as a numpy.ndarray in given range with
    given number of points.

    Parameters
    ----------
    fname : string
        File, filename, or generator to read. It assumes that the file has
        1D real-valued force array in Newtons.
    bounds : tuple, optional
        Bounds for force data using aligned z-sensor data, in nm.
        default = (-25, 100).
    n_points : integer, optional
        Number of points to return, it uses interpolation to get exact number
        of points.

    Returns
    -------
    force : (n_points, ) array_like
        1D real-valued force array loaded from given path in pN.

    """

    # Get the path and check what the extension is.
    ext = os.path.splitext(fname)[1]

    if ext.lower() == '.txt':

        # Load z-sensor and force data.
        zsnsr, force = np.loadtxt(fname, unpack=True)

    else:

        raise IOError('Unrecognized file type! Only .txt files are allowed.')

    # Interpolate force using z-sensor data.
    # Z-sensor data is reversed.
    # try:

    #     force_interp = interp1d(-zsnsr * 1e9, force * 1e12)
    #     result = force_interp(np.linspace(bounds[0], bounds[1], num=n_points))
    #     print result.shape

    # except ValueError:

    #     result = np.ones(n_points) * np.NaN

    return force[:n_points] * 1e12


def load_library(dir_path, bounds=(-25, 100), n_points=500):
    """
    Reads .txt files from a directory and returns them as variables. This
    routine assumes the first file in the directory is the guesses,
    alphanumerically. All files must have the same number of values, except
    the labels.

    Parameters
    ----------
    dir_path : string
        Path to directory that contains .txt files.
    bounds : tuple, optional
        Bounds for force data using aligned z-sensor data, in nm.
        default = (-25, 100).
    n_points : integer, optional
        Number of points to return, it uses interpolation to get exact number
        of points.

    Returns
    -------
    X : (n_samples, n_points) array_like
        Force data in picoNewtons, where n_samples is the number of samples.
    y : (n_samples, ) array_like
        Classes for the samples in X.

    See also
    --------
    io.load_signal : Loads a single force curve.

    """

    # Get the list of files in the directory and sort them.
    files = [os.path.join(dir_path, fname) for fname in os.listdir(dir_path)
             if os.path.isfile(os.path.join(dir_path, fname))]
    files.sort(key=__alphanum_key__)

    # Loop through files and load them.
    for i, fname in enumerate(files):

        # First file is the guesses.
        if i == 0:

            y = np.loadtxt(fname, dtype=int)

            # Do a sanity check.
            if not y.shape[0] == (len(files) - 1):
                print  y.shape[0]
                raise ValueError('Number of labels and signal ' +
                                 'files do not match!')

            # Everything is good, initialize the array.
            X = np.empty((y.shape[0], n_points))

        # Load the rest of the data.
        else:

            X[(i - 1), :] = load_signal(fname, bounds, n_points)

    return X, y

def load_predict_library(dir_path, bounds=(-25, 100), n_points=500):
    """
    Reads .txt files from a directory and returns them as variables. This
    routine assumes the first file in the directory is the guesses,
    alphanumerically. All files must have the same number of values.

    Parameters
    ----------
    dir_path : string
        Path to directory that contains .txt files.
    bounds : tuple, optional
        Bounds for force data using aligned z-sensor data, in nm.
        default = (-25, 100).
    n_points : integer, optional
        Number of points to return, it uses interpolation to get exact number
        of points.

    Returns
    -------
    X : (n_samples, n_points) array_like
        Force data in picoNewtons, where n_samples is the number of samples.

    See also
    --------
    io.load_signal : Loads a single force curve.

    """

    # Get the list of files in the directory and sort them.
    files = [os.path.join(dir_path, fname) for fname in os.listdir(dir_path)
             if os.path.isfile(os.path.join(dir_path, fname))]
    files.sort(key=__alphanum_key__)

    # Initialize the array.
    X = np.empty((len(files), n_points))

    # Loop through files and load them.
    for i, fname in enumerate(files):

        X[i, :] = load_signal(fname, bounds, n_points)


    return X

def load_libraries(dir_list, bounds=(-25, 100), n_points=500):
    """
    Loads different libraries and concatenates them.

    Parameters
    ----------
    dir_list : array
        List of directiories to load libraries from.
    bounds : tuple, optional
        Bounds for force data using aligned z-sensor data, in nm.
        default = (-25, 100).
    n_points : integer, optional
        Number of points to return, it uses interpolation to get exact number
        of points.

    Returns
    -------
    X : (n_samples_total, n_points) array_like
        Force data in picoNewtons, where n_samples is the number of samples.
    y : (n_samples_total, ) array_like
        Classes for the samples in X.

    See also
    --------
    io.load_library : Loads a single library.

    """

    # Start going through the list.
    for i, dir_path in enumerate(dir_list):

        X, y = load_library(dir_path, bounds, n_points)

        if i == 0:

            X0, y0 = np.copy(X), np.copy(y)

        else:

            X0 = np.vstack((X0, X))
            y0 = np.hstack((y0, y))

    return X0, y0
