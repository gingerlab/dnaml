"""dna.py: Includes routines for generating features, complies with
   scikit-learn templates."""

__author__ = "Durmus U. Karatay"
__copyright__ = "Copyright 2015, Ginger Lab"
__maintainer__ = "Durmus U. Karatay"
__email__ = "ukaratay@uw.edu"
__status__ = "Development"

import numpy as np
from scipy.signal import medfilt, savgol_filter, argrelmax
from scipy.stats import moment
from sklearn.preprocessing import scale

class ForcePeaks(object):
    """
    Feature generator that calculates the total force applied on a DNA
    strand and the number of peaks in a pulling curve.

    This feature generator calculates the baseline for force by averaging
    last 80 points in a pulling curve and subtracting it from the minimum
    point in that curve. The generator looks only at the features (X), not
    the desired outputs (y). Thus, it can be used for unsupervised learning.

    Parameters
    ----------
    level : float, optional
        The number of standard deviations that any force peak to be in to be
        counted as a peak. (default=4)
    scaled : boolean, optional
        Standardize outputs, center to the mean and component wise scale to
        unit variance. default: False.

    Attributes
    ----------
    force_ : (n_samples,) array_like
        The total force for every sample (in pN).
    n_peaks_ : (n_samples,) array_like
        The number of peaks in each curve.

    """

    def __init__(self, level=4.0 ,scaled=False):

        # 4 is not a magic number, it's where flat curves have small peaks.
        self.level = level
        self.scaled = scaled

        return

    def fit_transform(self, X, y=None):
        """Fit the model with X and calculate forces and number of peaks in X.

        Parameters
        ----------
        X : (n_samples, n_points), array_like
            Training data, where n_samples is the number of samples and
            n_features is the number of features.

        Returns
        -------
        X_new : (n_samples, 2), array_like
            The first column is force and the second column is number of peaks.
            Force is in pN. If scaled is True, both columns are scaled
            (no units).

        """
        bounds = (80 , 200)
        # Get the baseline and standard deviation from noise.
        # 20 is not a magic number, it's where the standard deviation stops
        # changing too much.

        baseline_avg = X[:, -20:].mean(axis=1)
        baseline_std = X[:, -20:].std(axis=1)

        # Get the minima and calculate forces.
        X_min = X[:,bounds[0]:bounds[1]].min(axis=1)
        self.force_ = baseline_avg - X_min

        # If calculated force is less than level * noise, it is flat.
        self.n_peaks_ = np.ones(self.force_.shape)
        self.n_peaks_[self.force_ < self.level * baseline_std] = 0

        # Find the indices for non-flat ones and go over them.
        idx = np.nonzero(self.n_peaks_)[0]

        for i in idx:

            # Don't want to overwrite the original.
            x = np.copy(X[i, :]) - baseline_avg[i]
            x[x > -(self.level * baseline_std[i] / 2)] = 0  # Zero noise.

            x = medfilt(x, 5)  # Still some noise, get rid of them.
            xd = savgol_filter(x, 11, 1, 1)  # First derivative.
            xd[xd < 1] = 0  # Removing unrelated makes peak-finding easier.
            
            xd = xd[bounds[0]:bounds[1]]

            # Find peaks. 13 is a magic number here.
            self.n_peaks_[i] = len(argrelmax(xd, order=13)[0])

        X_new = np.hstack((self.force_.reshape(X.shape[0], 1),
                           self.n_peaks_.reshape(X.shape[0], 1)))

        if self.scaled:

            X_new = scale(X_new, axis=0)

        return X_new

    def get_last_peak(self, X):
        """Get the value of last peak in X.

        Parameters
        ----------
        X : (n_samples, n_points), array_like
            Training data, where n_samples is the number of samples and
            n_features is the number of features.

        Returns
        -------
        X_new : (n_samples, 2), array_like
            The first column is force and the second column is number of peaks.
            Force is in pN. If scaled is True, both columns are scaled
            (no units).

        """

        # Get the baseline and standard deviation from noise.
        # 20 is not a magic number, it's where the standard deviation stops
        # changing much.
        baseline_avg = X[:, -20:].mean(axis=1)
        baseline_std = X[:, -20:].std(axis=1)

        # Get the minima and calculate forces.
        X_min = X.min(axis=1)
        self.force_ = baseline_avg - X_min

        # If calculated force is less than level * noise, it is flat.
        self.rupture_ = np.ones(self.force_.shape)
        self.rupture_[self.force_ < self.level * baseline_std] = 0

        # Find the indices for non-flat ones and go over them.
        idx = np.nonzero(self.rupture_)[0]

        for i in idx:

            # Don't want to overwrite the original.
            x = np.copy(X[i, :]) - baseline_avg[i]
            x[x > -(self.level * baseline_std[i] / 2)] = 0  # Zero noise.

            x = medfilt(x, 5)  # Still some noise, get rid of them.
            xd = savgol_filter(x, 11, 1, 1)  # First derivative.
            xd[xd < 1] = 0  # Removing unrelated makes peak-finding easier.

            try:

                # Find peaks. 13 is a magic number here.
                last_peak_idx = argrelmax(xd, order=13)[0][-1]
                self.rupture_[i] = -x[(last_peak_idx-13):last_peak_idx].min()

            except:

                self.rupture_[i] = 0

        return self.rupture_

    transform = fit_transform


class Moments(object):
    """
    Find central moments of every signal up to the given number of moments.
    Only calculates this for data points under baseline after noise removal.

    Parameters
    ----------
    moments : array_like, optional
        Central moments to calculate. default: (2, 3, 4)

    scaled : boolean, optional
        Standardize moments, center to the mean and component wise scale to
        unit variance. default: True.

    """

    def __init__(self, moments=(2, 3, 4), scaled=True):

        self.moments = moments
        self.n_moments_ = len(moments)
        self.scaled_ = scaled

        return

    def fit_transform(self, X, y=None):
        """
        Fit the model with X and calculate moments for x.

        Parameters
        ----------
        X : (n_samples, n_points), array_like
            Data, where n_samples is the number of samples and n_points is
            the number of points.

        Returns
        -------
        X_new: (n_samples, n_moments) array_like
            Moments of data in given order. n_moments is the number of moments
            calculated.

        """

        # Get the baseline and standard deviation.
        baseline_avg = X[:, -20:].mean(axis=1)
        baseline_std = X[:, -20:].std(axis=1)

        X_new = np.empty((X.shape[0], self.n_moments_))

        for i in range(X.shape[0]):

            x = X[i, :] - baseline_avg[i]  # Remove baseline.
            x[x > -(2 * baseline_std[i])] = 0  # Zero noise.
            x = medfilt(x, 7)  # Still some noise.

            # Calculate moments and put them in an array.
            for j in range(self.n_moments_):

                X_new[i, j] = moment(x, moment=self.moments[j])

        if self.scaled_:

            X_new = scale(X_new, axis=0)

        return X_new

    transform = fit_transform


class Slope(object):
    """
    Find slope of the line connecting starting point to the minimum point.

    Parameters
    ----------

    best_line : boolean, optional
        Uses best line to calculate the slope instead of point-to-point slope.
        default: True.

    """

    def __init__(self, best_line=True):

        self.best_line = best_line

        return

    def fit_transform(self, X, y=None):
        """
        Fit the model with X and calculate slopes for x.

        Parameters
        ----------
        X : (n_samples, n_points), array_like
            Data, where n_samples is the number of samples and n_points is
            the number of points.

        Returns
        -------
        X_new: (n_samples, 1) array_like
            Slope of data in given order.

        """
        bounds = (80 , 200)
        # Get the baseline and standard deviation.
        baseline_avg = X[:, -20:].mean(axis=1)

        X_argmin = X.argmin(axis=1)
        X_new = np.empty((X.shape[0], 1))

        for i in range(X.shape[0]):

            x = X[i, :] - baseline_avg[i]  # Remove baseline.

            # If the curve has no peak, we find where it reaches 0.
            if x.min() < 2 * x[-20:].std():

                X_argmin[i] = np.searchsorted(x, 0, sorter=x.argsort())

            # Best line is 1st-order polynomial fit.
            if self.best_line:

                p = np.polyfit(np.arange(X_argmin[i]), x[0:X_argmin[i]], 1)
                X_new[i] = p[0]

            else:

                X_new[i] =  (x[X_argmin[i]] - x[0]) / X_argmin[i]

        return X_new

    transform = fit_transform


class STD(object):
    """
    Find standard deviation of a slope-corrected peak, calculates until the
    largest peak. Uses the basic slope.

    """

    def __init__(self):

        return

    def fit_transform(self, X, y=None):
        """
        Fit the model with X and calculate standard deviation.

        Parameters
        ----------
        X : (n_samples, n_points), array_like
            Data, where n_samples is the number of samples and n_points is
            the number of points.

        Returns
        -------
        X_new: (n_samples, 1) array_like
            Calculated standard deviation.

        """
        bounds = (80 , 200)
        # Get the baseline and standard deviation.
        baseline_avg = X[:, -20:].mean(axis=1)

        X_argmin = X[:,bounds[0]:bounds[1]].argmin(axis=1)
        X_new = np.empty((X.shape[0], 1))

        for i in range(X.shape[0]):

            x = X[i, :] - baseline_avg[i]  # Remove baseline.

            if x.min() < 2 * x[-20:].std():

                X_argmin[i] = np.searchsorted(x, 0, sorter=x.argsort())

            z = np.arange(X_argmin[i])

            m = (x[X_argmin[i]] - x[0]) / X_argmin[i]

            X_new[i] = (x[0:X_argmin[i]] - m * z - x[0]).std()

        return X_new

    transform = fit_transform


class Area(object):
    """Find area underneath baseline after noise removal."""

    def __init__(self):

        return

    def fit_transform(self, X, y=None):
        """
        Fit the model with X and calculate area underneath baseline.

        Parameters
        ----------
        X : (n_samples, n_points), array_like
            Data, where n_samples is the number of samples and n_points is
            the number of points.

        Returns
        -------
        X_new: (n_samples, 1) array_like
            Moments of data in given order. n_moments is the number of moments
            calculated.

        """
        bounds = (80 , 200)
        # Get the baseline and standard deviation.
        baseline_avg = X[:, -20:].mean(axis=1)
        baseline_std = X[:, -20:].std(axis=1)

        X_new = np.empty((X.shape[0], 1))

        for i in range(X.shape[0]):

            x = X[i, :] - baseline_avg[i]  # Remove baseline.
            x[x > -(2 * baseline_std[i])] = 0  # Zero noise.
            x = medfilt(x, 7)  # Still some noise.
            x = x[bounds[0]:bounds[1]] # bound our integral

            # Integral here is just a sum.
            X_new[i] = -x.sum()
        

        return X_new

    transform = fit_transform
