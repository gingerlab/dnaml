import numpy as np
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Plotting parameters
mpl.style.use('aip')
dpi = 300
fmt = ['o', 'd', 'v', 's']
c = ['#A60628', '#348ABD', '#7A68A6']

# Width and height of the figures in inches.
w, w2 = 3.33, 7
h, h2, h3 = 2, 4, 6

def scatter_3d(X, y, axis=[0, 1, 2]):

    fig = plt.figure(figsize=(w, w), dpi=dpi)
    gs = gridspec.GridSpec(1, 1)
    ax = plt.subplot(gs[:, :], projection='3d')

    n_types = len(np.unique(y))

    for i in range(n_types):

        ax.scatter(X[y == i, axis[0]], X[y == i, axis[1]], X[y == i, axis[2]],
                   c=c[i], s=25, lw=0.25, label='{}'.format(i))

    ax.set_xlabel('Loading {}'.format(axis[0]))
    ax.set_ylabel('Loading {}'.format(axis[1]))
    ax.set_zlabel('Loading {}'.format(axis[2]))

    ax.legend(loc=0, framealpha=0, scatterpoints=1, ncol=1, fontsize='x-small')
    plt.tight_layout()

    return fig, ax


def scatter_2d(X, y, axis=[0, 1], title=None):

    fig = plt.figure(figsize=(w, w), dpi=dpi)
    gs = gridspec.GridSpec(1, 1)
    ax = plt.subplot(gs[:, :])

    n_types = len(np.unique(y))

    for i in range(n_types):

        if n_types <= 4:

            ax.scatter(X[y == i, axis[0]], X[y == i, axis[1]], marker=fmt[i],
                       c=c[i], s=50, lw=0.25, label='{}'.format(i))

        else:

            ax.scatter(X[y == i, axis[0]], X[y == i, axis[1]],
                       c=c[i], s=50, lw=0.25, label='{}'.format(i))


    ax.set_xlabel('Loading {}'.format(axis[0]))
    ax.set_ylabel('Loading {}'.format(axis[1]))

    ax.legend(loc=0, framealpha=0, scatterpoints=1, ncol=1, fontsize='x-small')

    if title is not None:

        ax.set_title(title)

    plt.tight_layout()

    return fig, ax
