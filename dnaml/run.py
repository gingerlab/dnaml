"""run.py: Runs Random Forest Classifier on given set of data."""

__author__ = "Durmus U. Karatay"
__copyright__ = "Copyright 2015, Ginger Lab"
__email__ = "ukaratay@uw.edu"
__status__ = "Production"

import os
import sys
import argparse as ap
import numpy as np
import dnaml.rfc as rfc
from dnaml.utils import io


def main(argv=None):
    """Main function of the executable file."""

    full_path = os.path.realpath(__file__)
    path, _ = os.path.split(full_path)
   
    if argv is None:
        argv = sys.argv[1:]

    # Parse arguments from the command line, and print out help.
    parser = ap.ArgumentParser(description='DNA-ML Prediction Software v1.0')

    parser.add_argument('-m', '--multiclass', help='multiclass classification',
                        action='store_true')
    parser.add_argument('prediction_path', nargs='?', help='path to predict')
    parser.add_argument('training_path', nargs='?', default=os.path.join(path, 'training'),
                        help='path to train, if not given, trains on default data')

    args = parser.parse_args(argv)
    # Get the training path and load it.
    X_train, y_train = io.load_library(args.training_path)

    # Get the prediction path and load it.
    X_predict = io.load_predict_library(args.prediction_path)

    r = rfc.RFC(multiclass=args.multiclass)
    r.train(X_train, y_train)
    y_pred = r.predict(X_predict)

    # Save txt files.
    os.chdir(args.prediction_path)
    np.savetxt('predictions.txt', y_pred, fmt='%d')

    return

if __name__ == '__main__':

    sys.exit(main(sys.argv[1:]))
